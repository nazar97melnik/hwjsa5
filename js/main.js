"use strict";

// - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//   - `https://ajax.test-danit.com/api/json/users`
//   - `https://ajax.test-danit.com/api/json/posts`
// - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
// - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`. При необхідності ви можете додавати також інші класи.
class Card {
  constructor(name, lastname, email, body, title, postId) {
    this.title = title;
    this.body = body;
    this.name = name;
    this.lastname = lastname;
    this.email = email;
    this.postId = postId;
  }

  createCard() {
    this.cardDiv = document.createElement("div");
    this.cardDiv.classList.add("post");
    document.body.append(this.cardDiv);
    this.cardDiv.innerHTML = `
    <div class="post-header">
    <img src="./images.png" width="20" height="20"/>
    <p>${this.name} ${this.lastname}</p>
    <p class="post-creator-email">${this.email}</p>
    </div>
    <a href="#" class="delete-btn"></a>
    <p class="post-title">${this.title}</p>
    <p class="post-body">${this.body}</p>
    `;
    const deleteBtn = this.cardDiv.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", this.handleDelete.bind(this));
    return this.cardDiv;
  }

  handleDelete(event) {
    event.preventDefault();
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "DELETE",
    })
      .then((response) => {
        const isConfirmed = confirm("Ви впевнені?");
        if (response.ok && isConfirmed) {
          this.cardDiv.remove();
        } else {
          alert("Помилка при видаленні");
        }
      })
      .catch((error) => {
        console.error("Помилка при видаленні", error);
      });
  }
}

Promise.all([
  fetch(`https://ajax.test-danit.com/api/json/users`),
  fetch(`https://ajax.test-danit.com/api/json/posts`),
])
  .then((responses) => {
    return Promise.all(
      responses.map((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(`Response error: ${response.status}`);
        }
      })
    );
  })
  .then(([users, posts]) => {
    const cards = posts.map((post) => {
      const user = users.find((user) => user.id === post.userId);
      const [firstName, lastName] = user.name.split(" ");
      return new Card(
        firstName,
        lastName,
        user.email,
        post.body,
        post.title,
        post.id
      );
    });
    cards.forEach((e) => {
      e.createCard();
    });
  })
  .catch((error) => {
    console.error(error);
  });
